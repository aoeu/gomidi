package main

import (
    "midi"
)

func pipeExample() {
    devices, _ := midi.GetDevices()
    nanoPad, _ := devices["nanoPAD2 PAD"]
	iac1, _ := devices["IAC Driver Bus 1"]
	pipe := midi.Pipe{nanoPad, iac1}
	pipe.Init()
	go pipe.Run() // Superfluous, but let's see that this works in a goroutine. 
    c := make(chan int)
    <-c // Block forever.
}

func routerExample() {
    devices, _ := midi.GetDevices()
    nanoPad, _ := devices["nanoPAD2 PAD"]
	iac1, _ := devices["IAC Driver Bus 1"]
    iac2, _ := devices["IAC Driver Bus 2"]
    router := midi.Router{nanoPad, []midi.Device{iac1, iac2}}
    router.Init()
    go router.Run()
    c := make(chan int)
    <-c // Block forever.
}

// This doesn't work like I would expect.
func chainExample() {
    devices, _ := midi.GetDevices()
    nanoPad, _ := devices["nanoPAD2 PAD"]
	iac1, _ := devices["IAC Driver Bus 1"]
    iac2, _ := devices["IAC Driver Bus 2"]
    chain := midi.Chain{Devices: []midi.Device{nanoPad, iac1, iac2}}
    chain.Init()
    go chain.Run()
    c := make(chan int)
    <-c // Block forever.
}

func transposerExample() {
    devices, _ := midi.GetDevices()
    nanoPad := devices["nanoPAD2 PAD"]
    transposer := &midi.Transposer{NoteMap: map[int]int{36: 37, 37:36}}
    iac1 := devices["IAC Driver Bus 1"]
    chain := midi.Chain{Devices: []midi.Device{nanoPad, transposer, iac1}}
    chain.Init()
    go chain.Run()
    c := make(chan int)
    <-c  // Block forever 
}

func main() {
    transposerExample()
}
