package midi
/*
Notes on the interfaces in this package:

A Port has go channels for reading / writing MIDI data
and may read / write from underlying system MIDI streams via C.
There are input ports (for output streams) and output ports 
(for input streams). A Port is to represent the physical 
MIDI in and MIDI out ports of devices, not the file streams
that the OS uses to transfers data to them.

A Connection is made by associating 2 or more Devices. 
A Connection is initialized by initializing its devices.
A Connection is run so data is parsed between devices.
On Connection implementations:
    Pipe: one to one connection for Devices.
    Router: one to many connection for Devices.
    Chain: a serial connection of an arbitrary number of Pipes.

A device is made with an input Port and / or an Output port.
A device is initialized by opening its Ports.
A device is run by running its Ports.
On Device implementations:
    SystemDevice: Real World MIDI devices plugged into the System
        (or software buses provided by the OS that emulate such.)
    TransposerDevice: A "fake" device that can be piped or chained
        to other devices in order to manipulate or transpose
        the MIDI data coming through it.
*/

// #cgo LDFLAGS: -lportmidi
// #include <portmidi.h>
import "C"
import (
	"errors"
	"time"
	"unsafe"
)

type Event struct {
	Channel int
	Command int
	Data1   int
	Data2   int
}

type Note struct {
	Channel  int
	Key      int
	Velocity int
}

type ControlChange struct {
	Channel int
	ID      int // a.k.a. Control Change "number"
	Value   int
}

func makePortMidiError(errNum C.PmError) error {
	msg := C.GoString(C.Pm_GetErrorText(errNum))
	if msg == "" {
		return nil
	}
	return errors.New(msg)
}

// Ports ----------------------------------------------------------------------

type Port interface {
	Open() error
	IsOpen() bool
	Run()
	NoteOns() chan Note
	NoteOffs() chan Note
	ControlChanges() chan ControlChange
}

// Implements Port, abstracting a system MIDI stream as a port.
type SystemPort struct {
	isOpen         bool
	IsInputPort    bool
	id             int
	stream         unsafe.Pointer
	noteOns        chan Note
	noteOffs       chan Note
	controlChanges chan ControlChange
}

func (s *SystemPort) Open() error {
	if s.isOpen || s.id == -1 {
		return nil
	}
	var errNum C.PmError
	if s.IsInputPort {
		// The input / output naming LOOKS backwards, but we're opening a 
		// portmidi "output stream" for input Ports and vice versa.
		errNum = C.Pm_OpenOutput(&(s.stream), C.PmDeviceID(s.id),
			nil, C.int32_t(512), nil, nil, 0)
	} else {
		errNum = C.Pm_OpenInput(&(s.stream), C.PmDeviceID(s.id),
			nil, C.int32_t(512), nil, nil)
	}
	if errNum == 0 {
		s.isOpen = true
		s.noteOns = make(chan Note)
		s.noteOffs = make(chan Note)
		s.controlChanges = make(chan ControlChange)
	}
	return makePortMidiError(errNum)
}

func (s SystemPort) IsOpen() bool {
	return s.isOpen
}

func (s SystemPort) Run() {
	if s.IsInputPort {
		// An input port receives data for the device,
		// so we write to the port.
		for {
			select {
			case noteOn := <-s.NoteOns():
				s.writeEvent(Event{noteOn.Channel, 144,
					noteOn.Key, noteOn.Velocity})
			case noteOff := <-s.NoteOffs():
				s.writeEvent(Event{noteOff.Channel, 128,
					noteOff.Key, noteOff.Velocity})
			case cc := <-s.ControlChanges():
				s.writeEvent(Event{cc.Channel, 176, cc.ID, cc.Value})

			}
		}
	} else {
		// A device's output port sends data to another device, 
		// so we read from the port.
		for {
			if dataAvailable, _ := s.poll(); dataAvailable {
				event, err := s.readEvent()
				if err != nil {
					continue
				}
				switch event.Command {
				case 144: // Note On
					s.NoteOns() <- Note{event.Channel, event.Data1, event.Data2}
				case 128: // Note Off
					s.NoteOffs() <- Note{event.Channel, event.Data1, event.Data2}
				case 176: // Control Change
					s.ControlChanges() <- ControlChange{event.Channel,
						event.Data1, event.Data2}
				}
			}
			time.Sleep(1 * time.Millisecond)
		}
	}
}

func (s SystemPort) NoteOns() chan Note {
	return s.noteOns
}

func (s SystemPort) NoteOffs() chan Note {
	return s.noteOffs
}

func (s SystemPort) ControlChanges() chan ControlChange {
	return s.controlChanges
}

func (s SystemPort) poll() (bool, error) {
	if s.IsInputPort == true || s.stream == nil {
		return false, errors.New("No input stream set on this SystemPort")
	}
	if s.IsOpen() == false {
		return false, errors.New("No input stream is open on this SystemPort.")
	}
	dataAvailable, err := C.Pm_Poll(s.stream)
	if err != nil {
		return false, err // Tried to read data, failed.
	}
	if dataAvailable > 0 {
		return true, nil // Data available.
	}
	return false, nil // No data available.
}

func (s SystemPort) readEvent() (event Event, err error) {
	if s.IsInputPort {
		err = errors.New("Can only write, not read from input SystemPort.")
		return Event{}, err
	}
	var buffer C.PmEvent
	// Only read one event at a time.
	eventsRead := int(C.Pm_Read(s.stream, &buffer, C.int32_t(1)))
	if eventsRead > 0 {
		status := int(buffer.message) & 0xFF
		event.Channel = int(status & 0x0F)
		event.Command = int(status & 0xF0)
		event.Data1 = int((buffer.message >> 8) & 0xFF)
		event.Data2 = int((buffer.message >> 16) & 0xFF)
		return event, nil
	}
	return Event{}, nil // Nothing to read.
}

func (s SystemPort) writeEvent(event Event) error {
	status := event.Command + event.Channel
	message := ((event.Data2 << 16) & 0xFF0000) |
		((event.Data1 << 8) & 0xFF00) | (status & 0xFF)
	buffer := C.PmEvent{C.PmMessage(message), 0}
	err := C.Pm_Write(s.stream, &buffer, C.int32_t(1))
	return makePortMidiError(err)
}

// Implements Port, prentinding to be a system port for transposed values. 
type TransposerPort struct {
	isOpen         bool
	noteOns        chan Note
	noteOffs       chan Note
	controlChanges chan ControlChange
}

func (t *TransposerPort) Open() error {
	t.isOpen = true
	t.noteOns = make(chan Note)
	t.noteOffs = make(chan Note)
	t.controlChanges = make(chan ControlChange)
	return nil
}

func (t TransposerPort) IsOpen() bool {
	return t.isOpen
}

func (t TransposerPort) Run() {
	// Do nothing, Run is handled by the Transposer.
}

func (t TransposerPort) NoteOns() chan Note {
	return t.noteOns
}

func (t TransposerPort) NoteOffs() chan Note {
	return t.noteOffs
}

func (t TransposerPort) ControlChanges() chan ControlChange {
	return t.controlChanges
}

// Devices --------------------------------------------------------------------

type Device interface {
	Init() error
	Run()
	InPort() Port  // Stuff going into the device is received on the InPort.
	OutPort() Port // Stuff coming from the device is sent from the OutPort.    
}

type SystemDevice struct { // Implements Device
	inPort  *SystemPort
	outPort *SystemPort
	Name    string
}

func (s SystemDevice) Init() error {
	err := s.InPort().Open()
	if err != nil {
		return err
	}
	err = s.OutPort().Open()
	return err
}

func (s SystemDevice) Run() {
	if s.InPort().IsOpen() {
		go s.InPort().Run()
	}
	if s.OutPort().IsOpen() {
		go s.OutPort().Run()
	}
}

func (s SystemDevice) InPort() Port {
	return s.inPort
}

func (s SystemDevice) OutPort() Port {
	return s.outPort
}

func getSystemDevices() (inputs, outputs []SystemDevice) {
	numDevices := int(C.Pm_CountDevices())
	for i := 0; i < numDevices; i++ {
		info := C.Pm_GetDeviceInfo(C.PmDeviceID(i))
		name := C.GoString(info.name)

		var isInputPort, isOutputPort, isOpen bool
		if info.output > 0 { // "output" means "output stream" in portmidi-speak.
			isInputPort = true // An OUTPUT stream is for an INPUT port.
		}
		if info.input > 0 { // "input" means "input stream" in portmidi-speak.
			isOutputPort = true // An INPUT stream is for an OUTPUT port.
		}
		if info.opened > 0 {
			isOpen = true
		}
		port := &SystemPort{isOpen: isOpen, id: i, IsInputPort: isInputPort}
		device := SystemDevice{Name: name}

		if isInputPort {
			device.inPort = port
			device.outPort = &SystemPort{isOpen: false, id: -1}
			inputs = append(inputs, device)
		} else if isOutputPort {
			device.outPort = port
			device.inPort = &SystemPort{isOpen: false, id: -1}
			outputs = append(outputs, device)
		}
	}
	return inputs, outputs
}

func GetDevices() (map[string]SystemDevice, error) {
	inputs, outputs := getSystemDevices()
	devices := make(map[string]SystemDevice, len(inputs)+len(outputs))

	// Pair devices that have both an input and an output, add all to system.
	for _, inDev := range inputs {
		for _, outDev := range outputs {
			if inDev.Name == outDev.Name {
				inDev.outPort = outDev.inPort
				outDev.inPort = inDev.outPort
				break
			}
		}
		devices[inDev.Name] = inDev
	}
	for _, outDev := range outputs {
		if _, ok := devices[outDev.Name]; !ok {
			devices[outDev.Name] = outDev
		}
	}
	errNum := C.Pm_Initialize()
	return devices, makePortMidiError(errNum)
}

// Implements Device
type Transposer struct {
	NoteMap map[int]int
	inPort  *TransposerPort
	outPort *TransposerPort
}

func (t *Transposer) Init() error {
	if t.inPort == nil {
		t.inPort = &TransposerPort{}
		t.inPort.Open()

		t.outPort = &TransposerPort{}
		t.outPort.Open()
	}
	return nil
}

func (t Transposer) Run() {
	for {
		select {
		case noteOn := <-t.InPort().NoteOns():
			key, ok := t.NoteMap[noteOn.Key]
			if ok {
				noteOn.Key = key
			}
			t.OutPort().NoteOns() <- noteOn
		case noteOff := <-t.InPort().NoteOffs():
			key, ok := t.NoteMap[noteOff.Key]
			if ok {
				noteOff.Key = key
			}
			t.OutPort().NoteOffs() <- noteOff
		case cc := <-t.InPort().ControlChanges():
			t.OutPort().ControlChanges() <- cc
		}
	}
}

func (t Transposer) InPort() Port {
	return t.inPort
}

func (t Transposer) OutPort() Port {
	return t.outPort
}

// Connections ----------------------------------------------------------------

type Connection interface {
	Init()
	Run()
}

// Implements Connection, one to one.
type Pipe struct {
	From Device
	To   Device
}

func (p Pipe) Init() error {
	err := p.From.Init()
	if err != nil {
		return err
	}
	err = p.To.Init()
	return err
}

func (p Pipe) Run() {
	input := p.From.OutPort()
	output := p.To.InPort()
	go p.From.Run()
	go p.To.Run()
	for {
		select {
		case noteOn := <-input.NoteOns():
			output.NoteOns() <- noteOn
		case noteOff := <-input.NoteOffs():
			output.NoteOffs() <- noteOff
		case cc := <-input.ControlChanges():
			output.ControlChanges() <- cc
		}
	}
}

// Implements Connection, one to many.
type Router struct {
	From Device
	To   []Device
}

func (r Router) Init() error {
	err := r.From.Init()
	if err != nil {
		return err
	}
	for _, to := range r.To {
		err = to.Init()
		if err != nil {
			return err
		}
	}
	return nil
}

func (r Router) Run() {
	go r.From.Run()
	for _, to := range r.To {
		go to.Run()
	}
	for {
		select {
		case noteOn := <-r.From.OutPort().NoteOns():
			go func() {
				for _, to := range r.To {
					to.InPort().NoteOns() <- noteOn
				}
			}()
		case noteOff := <-r.From.OutPort().NoteOffs():
			go func() {
				for _, to := range r.To {
					to.InPort().NoteOffs() <- noteOff
				}
			}()
		case cc := <-r.From.OutPort().ControlChanges():
			go func() {
				for _, to := range r.To {
					to.InPort().ControlChanges() <- cc
				}
			}()
		}
	}
}

// Implements Connection, serially chained pipes.
type Chain struct {
	Devices []Device
	pipes   []Pipe
}

func (c *Chain) Init() error {
	numDevices := len(c.Devices)
	c.pipes = make([]Pipe, numDevices-1)
	for i := 1; i < numDevices; i++ {
		pipe := Pipe{c.Devices[i-1], c.Devices[i]}
		c.pipes[i-1] = pipe
	}
	for _, pipe := range c.pipes {
		pipe.Init()
	}
	return nil
}

func (c Chain) Run() {
	for _, pipe := range c.pipes {
		go pipe.Run()
	}
}
