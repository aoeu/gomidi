// portmidi.go is a simple Go wrapper for portmidi.
// Function names and signatures are similar to C versions in portmidi.
// The difference is that inputs and outputs of functions utilize Go types,
// alleviating the user from casting to the underlying C types.
// A minimum amount of convenience functions are included.
// The alternative library, midi.go, follows "Go way of doing things."
package portmidi

// #cgo LDFLAGS: -lportmidi
//#include <portmidi.h>
//#include <stdint.h>
import "C"
import (
    "errors"
    "unsafe"
)

// PMEXPORT const char *Pm_GetErrorText( PmError errnum );
func GetErrorText(errNum C.PmError) string {
	switch n := int(errNum); n {
	case C.pmNoError:
		return ""
	case C.pmHostError:
		return "PortMidi: `Host error'"
	case C.pmInvalidDeviceId:
		return "PortMidi: `Invalid device ID'"
	case C.pmInsufficientMemory:
		return "PortMidi: `Insufficient memory'"
	case C.pmBufferTooSmall:
		return "PortMidi: `Buffer too small'"
	case C.pmBadPtr:
		return "PortMidi: `Bad pointer'"
	case C.pmInternalError:
		return "PortMidi: `Internal PortMidi Error'"
	case C.pmBufferOverflow:
		return "PortMidi: `Buffer overflow'"
	case C.pmBadData:
		return "PortMidi: `Invalid MIDI message Data'"
	case C.pmBufferMaxSize:
		return "PortMidi: `Buffer cannot be made larger'"
	default:
		return "PortMidi: `Illegal error number'"
	}
	return ""
}

func makeError(errNum C.PmError) error {
    msg := GetErrorText(errNum)
    if msg == "" {
        return nil
    }
    return errors.New(msg)
}

// PMEXPORT int Pm_CountDevices( void );
func CountDevices() int {
    return int(C.Pm_CountDevices())
}

// PMEXPORT PmDeviceID Pm_GetDefaultInputDeviceID( void );
func GetDefaultInputDeviceID() int {
    return int(C.Pm_GetDefaultInputDeviceID())
}

// PMEXPORT PmDeviceID Pm_GetDefaultOutputDeviceID( void );
func GetDefaultOutputDeviceID() int {
    return int(C.Pm_GetDefaultOutputDeviceID())
}

type DeviceInfo struct {
    Name     string
    API      string
    IsInput  bool
    IsOutput bool
    IsOpen   bool
}

// PMEXPORT const PmDeviceInfo* Pm_GetDeviceInfo( PmDeviceID id );
func GetDeviceInfo(deviceNum int) DeviceInfo {
    di := C.Pm_GetDeviceInfo(C.PmDeviceID(deviceNum))
    name := C.GoString(di.name)
    API := C.GoString(di.interf)
    var isInput, isOutput, isOpen bool
    if di.input == 1 {
        isInput = true
    }
    if di.output == 1 {
        isOutput = true
    }
    if di.opened == 1 {
        isOpen = true
    }
    return DeviceInfo{name, API, isInput, isOutput, isOpen}
}

/*
PMEXPORT PmError Pm_OpenInput( PortMidiStream** stream,
                PmDeviceID inputDevice,
                void *inputDriverInfo,
                int32_t bufferSize,
                PmTimeProcPtr time_proc,
                void *time_info );
*/
func OpenInput(stream *unsafe.Pointer, 
               deviceID int, 
               driverInfo unsafe.Pointer,  
               bufferSize int32,
               timeProc unsafe.Pointer,
               timeInfo unsafe.Pointer) error {
    errNum := C.Pm_OpenInput(stream, C.PmDeviceID(deviceID), driverInfo, 
                 C.int32_t(bufferSize), C.PmTimeProcPtr(timeProc), timeInfo)
    return makeError(errNum)
}

func OpenOutput(stream *unsafe.Pointer,
                deviceID int,
                driverInfo unsafe.Pointer,
                bufferSize int,
                timeProc unsafe.Pointer,
                timeInfo unsafe.Pointer,
                latency int) error {
    errNum := C.Pm_OpenOutput(stream, C.PmDeviceID(deviceID), driverInfo,
                C.int32_t(bufferSize), C.PmTimeProcPtr(timeProc),
                timeInfo, C.int32_t(latency))
    return makeError(errNum)
}

// PMEXPORT PmError Pm_SetFilter( PortMidiStream* stream, int32_t filters );
func SetFilter(stream unsafe.Pointer, filters int) error {
    err := C.Pm_SetFilter(stream, C.int32_t(filters))
    return makeError(err)
}

// PMEXPORT PmError Pm_SetChannelMask(PortMidiStream *stream, int mask);
func SetChannelMask(stream unsafe.Pointer, mask int) error {
    err := C.Pm_SetChannelMask(stream, C.int(mask))
    return makeError(err)
}

// PMEXPORT PmError Pm_Abort( PortMidiStream* stream );
func Abort(stream unsafe.Pointer) error {
    err := C.Pm_Abort(stream)
    return makeError(err)
}

// PMEXPORT PmError Pm_Close( PortMidiStream* stream );
func Close(stream unsafe.Pointer) error {
    err := C.Pm_Close(stream)
    return makeError(err)
}

// PmError Pm_Synchronize( PortMidiStream* stream );
func Synchronize(stream unsafe.Pointer) error {
    err := C.Pm_Synchronize(stream)
    return makeError(err)
}

/*
typedef int32_t PmMessage
typedef struct {
    PmMessage      message;
    PmTimestamp    timestamp;
} PmEvent; */
type Event struct {
    Message int
    Timestamp int
}

func (e *Event) toC() C.PmEvent {
    return C.PmEvent{ C.PmMessage((*e).Message),
                      C.PmTimestamp((*e).Timestamp) }
}

// PMEXPORT int Pm_Read( PortMidiStream *stream, PmEvent *buffer, int32_t length );
/*func Read(stream unsafe.Pointer, numEvents int) {
     buffer := make([]C.PmEvent, numEvents)
    eventsRead := C.Pm_Read(stream, &buffer, numEvents)
    var events []Event
    if eventsRead > 0 {
        for _, event := range buffer {
            events = append(events, Event{int(event.message), int(event.timestamp)})
        }
    }
    return events
}*/

// PMEXPORT PmError Pm_Poll( PortMidiStream *stream);
func Poll(stream unsafe.Pointer) (bool, error) {
    dataAvailable, err := C.Pm_Poll(stream)
    if err != nil {
        return false, err
    }
    if dataAvailable > 0 {
        return true, nil
    }
    return false, nil
}

// PMEXPORT PmError Pm_Write( PortMidiStream *stream, PmEvent *buffer, int32_t length );
// This probably won't work for more than one Event in events.
func Write(stream unsafe.Pointer, events []Event) error {
    // typedef int32_t PmMessage;
    // typedef PmTimestamp (*PmTimeProcPtr)(void *time_info);
    var buffer []C.PmEvent
    for _, event := range events {
        buffer = append(buffer, event.toC())
    }
    bufferStart := C.PmEvent(buffer[0])
    err := C.Pm_Write(stream, &bufferStart, C.int32_t(len(buffer)))
    return makeError(err)
}

// PMEXPORT PmError Pm_WriteShort( PortMidiStream *stream, PmTimestamp when, int32_t msg);
func WriteShort(stream unsafe.Pointer, when int, message int) error {
    err := C.Pm_WriteShort(stream, C.PmTimestamp(when), C.int32_t(message))
    return makeError(err)
}

// PMEXPORT PmError Pm_WriteSysEx( PortMidiStream *stream, PmTimestamp when, unsigned char *msg);
func WriteSysEx(stream unsafe.Pointer, when int, message byte) error {
    msg := C.uchar(message)
    err := C.Pm_WriteSysEx(stream, C.PmTimestamp(when), &msg)
    return makeError(err)
}
