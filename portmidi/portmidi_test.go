package portmidi_test

import (
    "portmidi"
    "testing"
)

func TestCountDevices(t *testing.T) {
    i := portmidi.CountDevices()
    if i < 4 {
        t.Errorf("Less than 4 devices: %d", i)
    }
}

func TestGetDefaultInputDeviceID(t *testing.T) {
    i := portmidi.GetDefaultInputDeviceID()
    if i != 0 {
        t.Errorf("Invalid default input device ID: %d", i)
    }
}

func TestGetDefaultOutputDeviceID(t *testing.T) {
    i := portmidi.GetDefaultOutputDeviceID()
    if i != 2 {
        t.Errorf("Invalid default output device ID: %d", i)
    }
}
